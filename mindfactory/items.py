# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from w3lib.html import remove_tags
from mindfactory.processors import url_base, nice_price, shorten_description, nice_key
import datetime
from scrapy.http import HtmlResponse


class MindfactoryProduct(scrapy.Item):
    origin = scrapy.Field()
    mf_id = scrapy.Field()
    ean = scrapy.Field()
    sku = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    # sold_count = scrapy.Field()
    rating = scrapy.Field()
    rating_count = scrapy.Field()
    url = scrapy.Field()
    description = scrapy.Field()
    last_seen = scrapy.Field()
    pass


class MindfactoryRamProduct(MindfactoryProduct):
    meta = scrapy.Field()
    # module_count = scrapy.Field()  # number of modules
    # total_capacity = scrapy.Field()  # total capacity of all modules
    # module_capacity = scrapy.Field()  # module capacity
    # memoryinterface = scrapy.Field()  # memory interface variant
    # packaging = scrapy.Field()  # packaging
    # mounting_form = scrapy.Field()  # mounting form
    # frequency = scrapy.Field()  # ram frequency
    # voltage = scrapy.Field()  # voltage rating
    # CL = scrapy.Field()  # CASLatency
    # tRCD = scrapy.Field()  # RAS to CAS Delay
    # tRP = scrapy.Field()  # RAS precharge time
    # tRAS = scrapy.Field()  # row active time
    # XMP = scrapy.Field()  # intel extreme profile support
    # SKU = scrapy.Field()  # SKU modelnumber
    # feautures = scrapy.Field()  # feautures
    # model_links = scrapy.Field()  # modulelinks
    # price_date = scrapy.Field()  # price at a given date
    pass


class MindfactoryProductLoader(ItemLoader):
    # default_input_processor = MapCompose(remove_tags)
    # default_output_processor = TakeFirst()
    def __init__(self, item, response):
        super().__init__(item, response)
        # determine rating
        self.add_value('origin', url_base(response.url))
        self.add_xpath('mf_id', '//span[@class="products-model"]', MapCompose(remove_tags))
        self.add_xpath('ean', '//span[@class="product-ean"]', MapCompose(remove_tags))
        self.add_xpath('sku', '//span[@class="sku-model"]', MapCompose(remove_tags))
        self.add_xpath('name', '//h1', MapCompose(remove_tags))
        self.add_xpath('price', '//span[@class="specialPriceText"]', MapCompose(remove_tags, nice_price))
        rating = len(response.css('.fixheight-gallery div.pstars .glyphicon.glyphicon-star')) \
            + (len(response.css('.fixheight-gallery div.pstars .glyphicon.glyphicon-star-half')) / 2)
        # self.add_xpath('sold_count', '//span[@class="pcountsold"]', MapCompose(remove_tags))
        self.add_value('rating', rating)
        self.add_xpath('rating_count', '//span[@itemprop="reviewCount"]', MapCompose(remove_tags))
        self.add_value('url', response.url)
        self.add_xpath('description', '//p[@itemprop="description"]', MapCompose(remove_tags, shorten_description))
        self.add_value('last_seen', datetime.datetime.now().strftime("%Y-%m-%d - %H:%M:%S"))


class MindfactoryRamProductLoader(MindfactoryProductLoader):
    def __init__(self, item, response):
        super().__init__(item, response)
        for row in response.css('#mainContent table.table.table-striped.table-hover tbody tr').extract():
            res = HtmlResponse(url=response.url, body=row, encoding='utf-8')
            self.add_value('meta', {nice_key(res): remove_tags(res.xpath('//td[2]').extract_first())})
