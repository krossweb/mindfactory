import scrapy
from mindfactory.items import MindfactoryRamProduct, MindfactoryRamProductLoader


class MindfactorySpider(scrapy.Spider):
    name = "mindfactory"
    debug = True

    def start_requests(self):
        url = 'https://www.mindfactory.de/Hardware/Arbeitsspeicher+(RAM)/DDR4+Module.html'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        categories = response.css('.cat_level_2 .background_maincat1 a').xpath('@href').extract()
        # uncomment to scrape all categories
        if not self.debug:
            for category in categories:
                yield scrapy.Request(url=category, callback=self.parse_category)
        else:
            yield scrapy.Request(url=categories[1], callback=self.parse_category)

    def parse_category(self, response):
        # get product detail page links
        products = response.css('a.phover-complete-link').xpath('@href').extract()
        if not self.debug:
            for product in products:
                yield scrapy.Request(url=product, callback=self.parse_product)
        else:
            yield scrapy.Request(url=products[1], callback=self.parse_product)

        # make pagination great again and navigate to next category page, if it exists
        is_first_page = not bool(response.css('.pagination span.glyphicon-chevron-left').extract_first())
        is_last_page = not bool(response.css('.pagination span.glyphicon-chevron-right').extract_first())
        # might be buggy :(
        is_single_page = len(response.css('.pagination > li > a').extract()) < 2
        next_url = ''
        if is_first_page and not is_single_page:
            next_url = response.url + '/page/2'
        else:
            if not is_last_page:
                current_page = int(response.url.split('/')[-1])
                t = response.url.split('/')
                t.pop()
                next_url = '/'.join(t) + '/' + str(current_page + 1)
        if next_url:
            print(next_url)
            yield scrapy.Request(url=next_url, callback=self.parse_category)

    def parse_product(self, response):
        l = MindfactoryRamProductLoader(item=MindfactoryRamProduct(), response=response)
        product = MindfactoryRamProduct(l.load_item())
        yield product
