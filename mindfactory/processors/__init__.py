# -*- coding: utf-8 -*-
from w3lib.html import remove_tags

def url_base(url):
    return str(url)\
        .replace('https://', '') \
        .replace('http://', '') \
        .replace('www', '')\
        .replace('//', '') \
        .replace('.de', '') \
        .replace('.com', '') \
        .replace('.', '') \
        .split('/')[0]


def nice_price(price):
    return price \
        .replace('\u20ac\u00a0', '') \
        .replace('*', '') \
        .replace(',', '.')


def shorten_description(description):
    if description == 'F\u00fcr diesen Artikel ist noch keine Produktbeschreibung vorhanden.':
        pass
    else:
        return description


def nice_key(response):
    key = remove_tags(response.xpath('//td[1]').extract_first()).lower() \
        .replace(':', '') \
        .replace(' ', '_') \
        .replace('ß', 'ss') \
        .replace('Ü', 'ue') \
        .replace('ü', 'ue') \
        .replace('Ä', 'ae') \
        .replace('ä', 'ae') \
        .replace('Ö', 'oe') \
        .replace('ö', 'oe') \
        .replace('.', '') \
        .replace('_(cl)', '') \
        .replace('_(trcd)', '') \
        .replace('_(trp)', '') \
        .replace('_(tras)', '')
    return key
