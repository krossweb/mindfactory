#!/usr/bin/env bash

[ -e storage/mindfactory.json ] && rm storage/mindfactory.json && echo "Deleted storage/mindfactory.json"

scrapy crawl mindfactory -o storage/mindfactory.json
